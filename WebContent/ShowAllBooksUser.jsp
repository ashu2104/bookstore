<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@page import="oracle.jdbc.driver.*"%>
<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.OracleDriver"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body bgcolor=lightgreen>

	<table align=center>
		<tr>
			<td><img class="image" src="book.png" alt="LOGO" height="50"
				width="50" /></td>
			<td><h1>Welcome to Book Store</h1></td>
		</tr>
	</table>
<% 
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	
	try
	{
		String driver = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@localhost:1521:XE";
		String user = "SYSTEM";
		String pass = "SYSADMIN";
		
		Class.forName(driver);
		conn = DriverManager.getConnection(url,user,pass);
		conn.setAutoCommit(false);
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}%>
		<form action="BookSelection.jsp">
		<table align = center border="1">
		<tr><th>BookId</th><th>BookName</th><th>BookPrice</th><th>BookAuthor</th></tr>
		
	<% //if(selection.equals("BookId"))			
		String query = ("SELECT BOOK_ID, BOOK_NAME, BOOK_AUTHOR, BOOK_PRICE FROM BOOKSTORE");
		pst = conn.prepareStatement(query);
		rs = pst.executeQuery();
		while(rs.next())
		{
			String BookId = rs.getString("BOOK_ID");
			String BookName = rs.getString("BOOK_NAME");
			String BookPrice = rs.getString("BOOK_PRICE");
			String BookAuthor = rs.getString("BOOK_AUTHOR");
			%>
			<tr><td><%=BookId%></td><td><%=BookName%></td><td><%=BookPrice%></td><td><%=BookAuthor%></td><td><input type="CheckBox" name="Select" value="<%=BookId%>"></td><td><input type="Submit" value="BUY"></td></tr>
		<% }
	%>
</table>
</form>
<!-- <form action="CheckBooks.jsp">
<input type="submit" value="GoBack">
</form>
 -->
 </body>
</html>