<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@page import="oracle.jdbc.driver.*"%>
<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.OracleDriver"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		String firstname = request.getParameter("FirstName");
		String lastname = request.getParameter("LastName");
		String gender = request.getParameter("gender");
		String phoneno = request.getParameter("PhoneNo");
		String Address = request.getParameter("Address");
		String City = request.getParameter("City");
		String State = request.getParameter("State");
		String Country = request.getParameter("Country");
		String username = request.getParameter("UserName");
		String Emailid = request.getParameter("EmailID");
		String Password = request.getParameter("Password");
		String zipcode = request.getParameter("ZipCode");

		System.out.println(firstname);
		System.out.println(lastname);
		System.out.println(gender);
		System.out.println(phoneno);
		System.out.println(Address);
		System.out.println(City);
		System.out.println(State);
		System.out.println(Country);
		System.out.println(username);
		System.out.println(Emailid);
		System.out.println(Password);
		System.out.println(zipcode);

		Connection conn = null;
		PreparedStatement pst = null;

		try {
			String driver = "oracle.jdbc.driver.OracleDriver";
			String url = "jdbc:oracle:thin:@localhost:1521:XE";
			String username2 = "SYSTEM";
			String password2 = "SYSADMIN";

			Class.forName(driver);

			conn = DriverManager.getConnection(url, username2, password2);
			conn.setAutoCommit(false);

		} catch (SQLException sqlEx) {
			System.out.println(sqlEx);
			try {
				conn.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		try {
			String query = ("INSERT INTO REGISTRATION (USERNAME,FIRSTNAME,LASTNAME,PHONENO,ADDRESS,CITY,STATE,COUNTRY,ZIPCODE,SEX,EMAILID,PASSWORD) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			pst = conn.prepareStatement(query);

			pst.setString(1, username);
			pst.setString(2, firstname);
			pst.setString(3, lastname);
			pst.setString(4, phoneno);
			pst.setString(5, Address);
			pst.setString(6, City);
			pst.setString(7, State);
			pst.setString(8, Country);
			pst.setString(9, zipcode);
			pst.setString(10, gender);
			pst.setString(11, Emailid);
			pst.setString(12, Password);
			pst.executeUpdate();

			conn.commit();
			out.println("Successfully Registered");
		%>
		<jsp:include page="LoginPage.jsp" />		
		<% } catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				conn.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	%>
</body>
</html>