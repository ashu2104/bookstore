<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@page import="oracle.jdbc.driver.*" %>
<%@page import="oracle.sql.*" %>
<%@page import="oracle.jdbc.driver.OracleDriver"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<% 
Connection conn = null;
PreparedStatement pst = null;

String Selection = request.getParameter("Removal");

try {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String user = "SYSTEM";
	String pass = "SYSADMIN";

	Class.forName(driver);

	conn = DriverManager.getConnection(url, user, pass);
	conn.setAutoCommit(false);

} catch (Exception e) {
	e.printStackTrace();
}

try {
	String query = ("DELETE BOOKSTORE WHERE BOOK_NAME=?");
	pst = conn.prepareStatement(query);
	
		pst.setString(1, Selection);
		pst.executeUpdate();

	out.println("Successfully Deleted the Book");
	%>
	
	<jsp:include page="HomePage.jsp" />	

<%
}catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		conn.close();
		pst.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
}
%>
</body>
</html>