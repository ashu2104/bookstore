<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@page import="oracle.jdbc.driver.*" %>
<%@page import="oracle.sql.*" %>
<%@page import="oracle.jdbc.driver.OracleDriver"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<% 		
		String BookID = request.getParameter("BookID");
		String BookName = request.getParameter("BookName");
		String BookAuthor = request.getParameter("BookAuthor");
		String BookPrice = request.getParameter("BookPrice");
		
		Connection conn = null;
		PreparedStatement pst = null;

		try {
			String driver = "oracle.jdbc.driver.OracleDriver";
			String url = "jdbc:oracle:thin:@localhost:1521:XE";
			String user = "SYSTEM";
			String pass = "SYSADMIN";

			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, pass);
			conn.setAutoCommit(false);

		} catch (Exception e) {
			e.printStackTrace();
		}
			try
			{
				String query = ("UPDATE BOOKSTORE SET BOOK_ID = ?, BOOK_NAME = ?, BOOK_AUTHOR = ?, BOOK_PRICE = ? WHERE BOOK_ID = ?");
				pst = conn.prepareStatement(query);

				pst.setString(1, BookID);
				pst.setString(2, BookName);
				pst.setString(3, BookAuthor);
				pst.setString(4, BookPrice);
				pst.setString(5, BookID);				
				pst.executeUpdate();

				out.println("Updated Successfully");
				
			%>
				<jsp:include page="HomePage.jsp" />
			<% } catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					conn.close();
					pst.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
%>
</body>
</html>