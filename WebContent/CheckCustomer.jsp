<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@page import="oracle.jdbc.driver.*" %>
<%@page import="oracle.sql.*" %>
<%@page import="oracle.jdbc.driver.OracleDriver"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%! String username;
	String password;	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%

	String username = request.getParameter("username");
	String password = request.getParameter("password");
	
	session.setAttribute("User", username);
	
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		String usernameDB = null;
		String passwordDB = null;

		try {
			String driver = "oracle.jdbc.driver.OracleDriver";
			String url = "jdbc:oracle:thin:@localhost:1521:XE";
			String username1 = "SYSTEM";
			String password1 = "SYSADMIN";
			
			Class.forName(driver);

			conn = DriverManager.getConnection(url, username1, password1);

			conn.setAutoCommit(false);
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
			conn.rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {

			String query = ("SELECT USERNAME, PASSWORD FROM REGISTRATION WHERE USERNAME = ?");
			pst = conn.prepareStatement(query);
			pst.setString(1, username);
			
			rs = pst.executeQuery();
			
			while (rs.next()) {
				usernameDB = rs.getString("USERNAME");
				passwordDB = rs.getString("PASSWORD");

			}
		} catch (SQLException sqlEx) {
			System.out.println("Problem while executing sql");
			sqlEx.printStackTrace();

		} catch (Exception ex) {
			System.out.println("Unknown exception.");

		} finally {
			try {
				rs.close();
				pst.close();
				conn.close();
			} catch (SQLException sqlExce) {
				System.out.println("Problem while closing the connection.");
			}
		}
		
	if (username.equals(usernameDB) && password.equals(passwordDB))
	{ %>
		<jsp:forward page="CheckBooks.jsp" />				
	<%} 
	else
	{ %>
		<jsp:forward page="RegistrationPage.jsp" />
	<% } 	
%>
</body>
</html>