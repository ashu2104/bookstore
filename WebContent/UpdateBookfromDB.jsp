<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@page import="oracle.jdbc.driver.*"%>
<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.OracleDriver"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body bgcolor=lightgreen>
	<table align=center>
		<tr>
			<td><img class="image" src="book.png" alt="LOGO" height="50"
				width="50" /></td>
			<td><h1>Book Store</h1></td>
		</tr>
	</table>
<% 
	Connection conn = null;
	PreparedStatement pst = null;
	ResultSet rs = null;
	String Selection = request.getParameter("Updation");

try {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String user = "SYSTEM";
	String pass = "SYSADMIN";

	Class.forName(driver);

	conn = DriverManager.getConnection(url, user, pass);
	conn.setAutoCommit(false);

} catch (Exception e) {
	e.printStackTrace();
}

try {
	String query = ("SELECT BOOK_ID, BOOK_NAME, BOOK_AUTHOR, BOOK_PRICE FROM BOOKSTORE WHERE BOOK_ID = ?");
	pst = conn.prepareStatement(query);
	
		pst.setString(1, Selection);
		rs = pst.executeQuery();
		%>
			<form action="UpdatetoDB.jsp">
			<table id="Booklist" align=center border="1">
				<tr><th>BookID</th><th>BookName</th><th>BookAuthor</th><th>BookPrice</th></tr>
						
		<% while(rs.next())
		{
			String BookID = rs.getString("BOOK_ID");
			String BookName = rs.getString("BOOK_NAME");
			String BookAuthor = rs.getString("BOOK_AUTHOR");
			String BookPrice  = rs.getString("BOOK_PRICE");
			
		%> 
				<tr><td><input type="text" value=<%=BookID%> name="BookID"> </td>
				<td><input type="text" value=<%=BookName%> name="BookName"></td>
				<td><input type="text" value=<%=BookAuthor%> name="BookAuthor"></td>
				<td><input type="text" value=<%=BookPrice%> name="BookPrice"></td></tr>			
			<% }
		%>
		<tr><td><input type="submit" value="Update"></td></tr>
		</table>
		</form>		
<%
}catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		conn.close();
		pst.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
}
%>
</body>
</html>