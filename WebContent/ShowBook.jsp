<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@page import="oracle.jdbc.driver.*"%>
<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.OracleDriver"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body bgcolor=lightgreen>
	<table align=center>
		<tr>
			<td><img class="image" src="book.png" alt="LOGO" height="50"
				width="50" /></td>
			<td><h1>Book Store</h1></td>
		</tr>
	</table>
		<%
			Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try
		{
			String driver = "oracle.jdbc.driver.OracleDriver";
			String url = "jdbc:oracle:thin:@localhost:1521:XE";
			String user = "SYSTEM";
			String pass = "SYSADMIN";
			
			Class.forName(driver);
			
			conn = DriverManager.getConnection(url,user,pass);
			
			conn.setAutoCommit(false);			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
			try
			{
			String query =("SELECT * FROM BOOKSTORE");
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
		%>
			<form action="ShowBookfromDB.jsp">
				<table align=center>
					<tr><th>Available Books</th></tr>
		<% 	while(rs.next())
			{
				String BookName = rs.getString("BOOK_NAME");		
			%>
				<tr><td><%=BookName%><input type="Radio" name="Show" value=<%=BookName%>></td></tr>
			<% }
		%>
			<tr><td><input type="submit" name="Submit" value="Submit"></td></tr>
			</table>
			</form>
		<% }
			catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				conn.close();
				pst.close();
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		%>
</body>
</html>