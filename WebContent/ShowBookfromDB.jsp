<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@page import="oracle.jdbc.driver.*" %>
<%@page import="oracle.sql.*" %>
<%@page import="oracle.jdbc.driver.OracleDriver"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
</body>
<body bgcolor=lightgreen>
 	<table align=center>
		<tr>
			<td><img class="image" src="book.png" alt="LOGO" height="50"
				width="50" /></td>
			<td><h1>Book Store</h1></td>
		</tr>
	</table>
<% 
Connection conn = null;
PreparedStatement pst = null;
ResultSet rs = null;
String Selection = request.getParameter("Show");

try {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String user = "SYSTEM";
	String pass = "SYSADMIN";

	Class.forName(driver);

	conn = DriverManager.getConnection(url, user, pass);
	conn.setAutoCommit(false);

} catch (Exception e) {
	e.printStackTrace();
}

try {
	String query = ("SELECT BOOK_ID, BOOK_NAME, BOOK_AUTHOR, BOOK_PRICE FROM BOOKSTORE WHERE BOOK_NAME = ?");
	pst = conn.prepareStatement(query);
	
		pst.setString(1, Selection);
		rs = pst.executeQuery();
		%>
			<table id="Booklist" align=center border="1">
				<tr><th>BookID</th><th>BookName</th><th>BookAuthor</th><th>BookPrice</th></tr>
						
		<% while(rs.next())
		{
			String BookID = rs.getString("BOOK_ID");
			String BookName = rs.getString("BOOK_NAME");
			String BookAuthor = rs.getString("BOOK_AUTHOR");
			String BookPrice  = rs.getString("BOOK_PRICE");
			
		%> 
				<tr><td><%=BookID%></td><td><%=BookName%></td><td><%=BookAuthor%></td><td><%=BookPrice%></td></tr>
			
			<% }
		%></table>		
<%
}catch (Exception e) {
	e.printStackTrace();
} finally {
	try {
		conn.close();
		pst.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
}
%>
<form action="HomePage.jsp">
<input type="Submit" Value="GOTO HOMEPAGE">
</form>
</body>
</html>