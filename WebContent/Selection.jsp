<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<% 
	String selection = request.getParameter("Selection");
	
	if(selection.equals("InsertBook")){ %>
		<jsp:forward page="InsertBook.jsp" />
	<% } 
	else if(selection.equals("UpdateBook"))
		{ %>
			<jsp:forward page="UpdateBook.jsp" />
		<% } 
	else if(selection.equals("RemoveBook"))
		{ %>
			<jsp:forward page="RemoveBook.jsp" />		
		<% } 
		else if(selection.equals("ShowBook"))
		{ %>
			<jsp:forward page="ShowBook.jsp" />
		<% } 
		else if(selection.equals("ShowAllBooks"))
		{ %>
			<jsp:forward page="ShowAllBooks.jsp" />
		<% }
		else
		{ %>
			<jsp:forward page="ThankYou.jsp" />	
		<% } %>
</head>
<body>
</body>
</html>