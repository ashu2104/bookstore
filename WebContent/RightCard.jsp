<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*"%>
<%@page import="oracle.jdbc.driver.*"%>
<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.OracleDriver"%>

    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body bgcolor="lightgreen">

<% 
	ServletContext context = request.getServletContext();
	String BookId = (String) context.getAttribute("BookSelect");
	
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try
		{
			String driver = "oracle.jdbc.driver.OracleDriver";
			String url = "jdbc:oracle:thin:@localhost:1521:XE";
			String user = "SYSTEM";
			String pass = "SYSADMIN";
			
			Class.forName(driver);
			conn = DriverManager.getConnection(url,user,pass);
			conn.setAutoCommit(false);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		try
		{
			String query = ("DELETE BOOKSTORE WHERE BOOK_ID=?");
			
			pst = conn.prepareStatement(query);
			
			pst.setString(1,BookId);
			pst.executeUpdate();
			
			out.println("Successfully Purchased");
		%> 
		<jsp:include page="CheckBooks.jsp" />	
		<%}catch(Exception e)
		{
			e.printStackTrace();
		}
%>
</body>
</html>